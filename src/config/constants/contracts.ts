import MigrateNFT from '../abis/MigrateNFT.json';
import Token from '../abis/Token.json';
import Game from '../abis/Game.json';
import NFT from '../abis/NFT.json';
import StaminaCompensation from '../abis/StaminaCompensation.json';
import { ChainId } from '.';
import { AbiItem } from 'web3-utils';

export const contracts = {
  game: {
    abi: Game as AbiItem[],
    address: {
      [ChainId.MAINNET]:
        process.env.REACT_APP_GAME_CONTRACT_ADDRESS_MAINNET || '',
      [ChainId.TESTNET]:
        process.env.REACT_APP_GAME_CONTRACT_ADDRESS_TESTNET || '',
    },
  },
  nft: {
    abi: NFT as AbiItem[],
    address: {
      [ChainId.MAINNET]:
        process.env.REACT_APP_NFT_CONTRACT_ADDRESS_MAINNET || '',
      [ChainId.TESTNET]:
        process.env.REACT_APP_NFT_CONTRACT_ADDRESS_TESTNET || '',
    },
  },
  token: {
    abi: Token as AbiItem[],
    address: {
      [ChainId.MAINNET]:
        process.env.REACT_APP_TOKEN_CONTRACT_ADDRESS_MAINNET || '', // real
      [ChainId.TESTNET]:
        process.env.REACT_APP_TOKEN_CONTRACT_ADDRESS_TESTNET || '',
    },
  },
  migrate_nft: {
    abi: MigrateNFT as AbiItem[],
    address: {
      [ChainId.MAINNET]:
        process.env.REACT_APP_MIGRATE_NFT_CONTRACT_ADDRESS_MAINNET || '', // real
      [ChainId.TESTNET]:
        process.env.REACT_APP_MIGRATE_NFT_CONTRACT_ADDRESS_TESTNET || '',
    },
  },
  staminnaCompensation: {
    abi: StaminaCompensation as AbiItem[],
    address: {
      [ChainId.MAINNET]:
        process.env.REACT_APP_STAMINA_COMPENSATION_CONTRACT_ADDRESS_MAINNET ||
        '',
      [ChainId.TESTNET]:
        process.env.REACT_APP_STAMINA_COMPENSATION_CONTRACT_ADDRESS_TESTNET ||
        '',
    },
  },
};
