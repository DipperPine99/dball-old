export enum ChainId {
  MAINNET = 56,
  TESTNET = 97,
}

export const BASE_EXCHANGE_URL = {
  [ChainId.MAINNET]:
    'https://exchange.pancakeswap.finance/#/swap?outputCurrency=%OUTPUT_CURRENTCY%',
  [ChainId.TESTNET]:
    'https://pancake.kiemtienonline360.com/#/swap?outputCurrency=%OUTPUT_CURRENTCY%',
};

export const DEFAULT_GAS_LIMIT = 400000;
export const DEFAULT_GAS_PRICE = 15;
export const HERO_COUNT = 4;
export const RARE_COUNT = 4;
export const AVAILABLE_VILLAIN_COUNT = 7;
export const AVAILABLE_HERO_COUNT = 8;
