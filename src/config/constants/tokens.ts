import { ChainId } from '.';
import { contracts } from './contracts';

const tokens = {
  dball: {
    symbol: 'DBALL',
    address: {
      56: contracts.token.address[ChainId.MAINNET],
      97: contracts.token.address[ChainId.TESTNET],
    },
    decimals: 18,
    projectLink: 'https://cryptodball.io/',
  },
};

export default tokens;
