import { createGlobalStyle } from 'styled-components';
import background from 'assets/images/background.png';
import flat from '../assets/images/flat.svg';
import '../assets/fonts/playbill.ttf';
import './global-styles.css';
import 'react-toastify/dist/ReactToastify.css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import 'bootstrap/dist/css/bootstrap.min.css';

export const GlobalStyle = createGlobalStyle`
  html,
  body {
    min-height: 100%;
    width: 100%;
    background: url(${flat}) no-repeat right bottom, url(${background}) no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
  }

  body {
    font-family: 'Merriweather', sans-serif;
  }

  #root {
    min-height: 100%;
    min-width: 100%;
  }

  p,
  label {
    font-family: Georgia, Times, 'Times New Roman', serif;
    line-height: 1.5em;
  }

  input, select {
    font-family: inherit;
    font-size: inherit;
  }
`;
