import BigNumber from 'bignumber.js';
import { getVillainImageByKey } from 'utils/assetsHelper';
import { ChallengerInterface } from 'utils/types';
import defaultVillianImage from 'assets/images/capsule.jpg';

const classNameMapping = {
  1: 'Bug Robot',
  2: 'Unfated',
  3: 'Green Alien',
  4: 'Gemini',
  5: 'Forgetten Solider',
  6: 'Drake Killer',
  7: 'Darkest Villain',
};

export class ChallengerModel implements ChallengerInterface {
  id!: number;
  name?: string;
  classId?: number;
  power?: number;
  expMax?: number;
  expMin?: number;
  rewardMax?: BigNumber;
  rewardMin?: BigNumber;
  winRate?: number;

  getImageUrl() {
    if (this.classId) {
      return getVillainImageByKey(this.classId);
    } else {
      return defaultVillianImage;
    }
  }

  getName() {
    if (this.classId && classNameMapping[this.classId]) {
      return classNameMapping[this.classId];
    } else {
      return `Challenger #${this.id}`;
    }
  }
}
