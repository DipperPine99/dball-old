import { getHeroImageByKey } from 'utils/assetsHelper';
import { CharacterInterface } from 'utils/types';
import capsuleImage from 'assets/images/capsule.png';

const classNameMapping = {
  1: 'Drake Warrior',
  2: 'Human Drake',
  3: 'Son of Drake',
  4: 'Drake Solider',
  5: 'Drake Swordsman',
  6: 'Little Son of Drake',
  7: 'Drake Keeper',
  8: 'Drake God',
};

export class CharacterModel implements CharacterInterface {
  id!: number;
  name?: string;
  stamina?: number;
  exp?: number;
  rare?: number;
  level?: number;
  hatched?: boolean;
  classId?: number;
  price?: number;
  extraStamina?: number;

  isHatched() {
    return this.hatched;
  }

  getImageClass() {
    return (this.id % 8) + 1;
  }

  getImageUrl() {
    if (this.classId && this.rare) {
      return getHeroImageByKey(this.classId, this.rare);
    }
    return capsuleImage;
  }

  getName() {
    if (this.classId && classNameMapping[this.classId]) {
      return `${classNameMapping[this.classId]} `;
    } else {
      return `Capsule`;
    }
  }

  changeHatched() {
    this.hatched = true;
  }
}
