import { contracts } from 'config/constants/contracts';
import { getAddress } from 'utils/addressHelpers';
import { CharacterInterface } from 'utils/types';

class CharactersService {
  async balanceOf(account: string) {
    const Contract = (window as any).web3.eth.Contract;
    const instance = new Contract(
      contracts.nft.abi,
      getAddress(contracts.nft.address),
    );
    try {
      const result: number = await instance.methods.balanceOf(account).call();
      return result;
    } catch (e) {
      console.error('cannot get NFT count', e);
    }
  }

  async getOwnedTokenIds(account: string) {
    const Contract = (window as any).web3.eth.Contract;
    const instance = new Contract(
      contracts.nft.abi,
      getAddress(contracts.nft.address),
    );
    try {
      const result: string = await instance.methods.balanceOf(account).call();
      const heroCount = parseInt(result);
      const characters: CharacterInterface[] = [];
      for (let i = 0; i < heroCount; i++) {
        const result: string = await instance.methods
          .tokenOfOwnerByIndex(account, i)
          .call();
        characters.push({
          id: parseInt(result),
        });
      }
      return characters;
    } catch (e) {
      console.error('cannot get ownedTokenIds', e);
    }
  }

  async listNFTOf(account: string, page: number = 1) {
    const web3 = (window as any).web3;
    const Contract = web3.eth.Contract;
    const instance = new Contract(
      contracts.nft.abi,
      getAddress(contracts.nft.address),
    );
    try {
      const result: string[] = await instance.methods
        .listNFTOf(account, page)
        .call();
      const characters: CharacterInterface[] = [];
      result.forEach((_id, i) => {
        if (i >= 4) {
          return;
        }
        const id = parseInt(_id);
        if (id) {
          characters.push({
            id,
          });
        }
      });
      return characters;
    } catch (e) {
      console.error('error getting listNFTOf', e);
    }
  }
}

export const charactersService = new CharactersService();
