import { StaminaCompensationState } from 'app/slice/types';
import { contracts } from 'config/constants/contracts';
import { getAddress } from 'utils/addressHelpers';

class CompensationService {
  async staminaCompensationCount(
    account: string,
  ): Promise<StaminaCompensationState> {
    const Contract = (window as any).web3.eth.Contract;
    const instance = new Contract(
      contracts.staminnaCompensation.abi,
      getAddress(contracts.staminnaCompensation.address),
    );
    try {
      const {
        0: isClaimable,
        1: claimed,
        2: maxClaim,
        3: nextClaim,
      } = await instance.methods.checkClaimable(account).call();
      return {
        isClaimable: isClaimable as boolean,
        claimed: parseInt(claimed),
        maxClaim: parseInt(maxClaim),
        remaining: parseInt(maxClaim) - parseInt(claimed),
        nextClaim: parseInt(nextClaim),
      } as StaminaCompensationState;
    } catch (e) {
      return {
        isClaimable: false,
        claimed: 0,
        maxClaim: 0,
        remaining: 0,
        nextClaim: 0,
      } as StaminaCompensationState;
    }
  }

  async claimStamina(account) {
    const Contract = (window as any).web3.eth.Contract;
    const instance = new Contract(
      contracts.staminnaCompensation.abi,
      getAddress(contracts.staminnaCompensation.address),
    );
    try {
      const result = await instance.methods.claimStamina().send({
        from: account,
      });
      return result;
    } catch (e) {
      console.error('Cannot claim stamina', e);
    }
  }
}

export const compensationService = new CompensationService();
