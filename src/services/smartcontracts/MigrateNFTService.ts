import { contracts } from 'config/constants/contracts';
import { getAddress } from 'utils/addressHelpers';

class MigrateNFTService {
  async isNeededToMigrate(account: string) {
    const Contract = (window as any).web3.eth.Contract;
    const instance = new Contract(
      contracts.migrate_nft.abi,
      getAddress(contracts.migrate_nft.address),
    );
    try {
      const result: string[] = await instance.methods.tokenList(account).call();
      let needMigrate: boolean = false;
      for (let i = 0; i < 4; i++) {
        const val = parseInt(result[i]);
        if (val > 0) {
          needMigrate = true;
          break;
        }
      }
      return needMigrate;
    } catch (e) {
      return false;
    }
  }

  async migrateNFT(account: string) {
    const Contract = (window as any).web3.eth.Contract;
    const instance = new Contract(
      contracts.migrate_nft.abi,
      getAddress(contracts.migrate_nft.address),
    );
    try {
      const result = await instance.methods
        .migrate()
        .send({ from: account, gas: 1500000 });
      console.log('result', result);
      return true;
    } catch (e) {
      console.error('cannot migrateNFT', e);
    }
  }
}

export const migrateNFTService = new MigrateNFTService();
