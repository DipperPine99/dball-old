import { contracts } from 'config/constants/contracts';
import { getAddress } from 'utils/addressHelpers';
import { store } from '../../store/configureStore';
import { ethers } from 'ethers';
import { DEFAULT_GAS_LIMIT } from 'config/constants';

class TokenService {
  async balanceOf(account: string) {
    const Contract = (window as any).web3.eth.Contract;
    const instance = new Contract(
      contracts.token.abi,
      getAddress(contracts.token.address),
    );
    try {
      const result: number = await instance.methods.balanceOf(account).call();
      return result;
    } catch (e) {
      console.error('cannot get balance of token', e);
    }
  }

  async getAllowance() {
    const wallet = store.getState().app.wallet;
    const Contract = (window as any).web3.eth.Contract;
    const instance = new Contract(
      contracts.token.abi,
      getAddress(contracts.token.address),
    );
    try {
      const result: string = await instance.methods
        .allowance(wallet.account, getAddress(contracts.game.address))
        .call();
      return parseInt(result);
    } catch (e) {
      console.error('cannot get allowance', e);
    }
  }

  async approve(cb?: (txHash: string) => void) {
    const wallet = store.getState().app.wallet;
    const web3 = (window as any).web3;
    const Contract = web3.eth.Contract;
    const amount = ethers.utils.parseUnits('1000000000.0', 18);
    // const amount = new BigNumber(
    //   '0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff',
    // );
    const instance = new Contract(
      contracts.token.abi,
      getAddress(contracts.token.address),
    );
    const result = await instance.methods
      .approve(getAddress(contracts.game.address), amount)
      .send({ from: wallet.account, gas: DEFAULT_GAS_LIMIT })
      .on('transactionHash', hash => {
        if (cb) {
          cb(hash);
        }
      });
    return result;
  }
}

export const tokenService = new TokenService();
