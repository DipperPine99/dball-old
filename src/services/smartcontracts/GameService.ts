import { contracts } from 'config/constants/contracts';
import { getAddress } from 'utils/addressHelpers';
import {
  ChallengerInterface,
  CharacterInterface,
  FightOutcome as FightOutcomeEventInterface,
} from 'utils/types';
import { store } from '../../store/configureStore';
import { BigNumber } from 'bignumber.js';
import { DEFAULT_GAS_LIMIT } from 'config/constants';

class GameService {
  async evolve(cb?: (txHash: string) => void) {
    const wallet = store.getState().app.wallet;
    const Contract = (window as any).web3.eth.Contract;
    const instance = new Contract(
      contracts.game.abi,
      getAddress(contracts.game.address),
    );
    const result = await instance.methods
      .evolve()
      .send({ from: wallet.account, gas: DEFAULT_GAS_LIMIT })
      .on('transactionHash', hash => {
        if (cb) {
          cb(hash);
        }
      });
    return result;
  }

  async combat(token: number, rate: number, cb?: (txHash: string) => void) {
    const wallet = store.getState().app.wallet;
    const Contract = (window as any).web3.eth.Contract;
    const instance = new Contract(
      contracts.game.abi,
      getAddress(contracts.game.address),
    );
    const result = await instance.methods
      .combat(token, rate)
      .send({ from: wallet.account, gas: DEFAULT_GAS_LIMIT })
      .on('transactionHash', hash => {
        if (cb) {
          cb(hash);
        }
      });

    if (result?.events?.FightOutcome) {
      const fightOutcome: FightOutcomeEventInterface = {
        isWin: result.events.FightOutcome.returnValues.isWin,
        ballGain: new BigNumber(
          result.events.FightOutcome.returnValues.ballGain,
        ),
        target: parseInt(result.events.FightOutcome.returnValues.target),
        xpGain: parseInt(result.events.FightOutcome.returnValues.xpGain),
        _tokenId: parseInt(result.events.FightOutcome.returnValues._tokenId),
      };
      return fightOutcome;
    }
  }

  async hatch(id: number, cb?: (txHash: string) => void) {
    const wallet = store.getState().app.wallet;
    const Contract = (window as any).web3.eth.Contract;
    const instance = new Contract(
      contracts.game.abi,
      getAddress(contracts.game.address),
    );
    const result = await instance.methods
      .hatch(id)
      .send({ from: wallet.account, gas: DEFAULT_GAS_LIMIT })
      .on('transactionHash', hash => {
        if (cb) {
          cb(hash);
        }
      });
    return result;
  }

  async getInfo(tokenId: number) {
    const Contract = (window as any).web3.eth.Contract;
    const instance = new Contract(
      contracts.game.abi,
      getAddress(contracts.game.address),
    );
    try {
      const {
        0: rare,
        1: exp,
        2: level,
        3: classId,
        4: hatched,
        5: stamina,
        6: price,
      } = (await instance.methods.getInfo(tokenId).call()) as {
        0: string;
        1: string;
        2: string;
        3: string;
        4: boolean;
        5: string;
        6: string;
      };
      return {
        rare: parseInt(rare),
        exp: parseInt(exp),
        level: parseInt(level),
        classId: parseInt(classId),
        hatched,
        stamina: parseInt(stamina),
        price: parseInt(price),
      } as CharacterInterface;
    } catch (e) {
      console.error('cannot get info', e);
    }
  }

  async getExtraStaminaOf(tokenId: number) {
    const web3 = (window as any).web3;
    const Contract = web3.eth.Contract;
    const instance = new Contract(
      contracts.game.abi,
      getAddress(contracts.game.address),
    );
    try {
      const result: string = await instance.methods
        .getExtraStaminaOf(tokenId)
        .call();
      return parseInt(result);
    } catch (e) {
      console.error('error getting getExtraStaminaOf', e);
    }
  }

  async listCharacterOf(account: string, page: number = 1) {
    const web3 = (window as any).web3;
    const Contract = web3.eth.Contract;
    const instance = new Contract(
      contracts.game.abi,
      getAddress(contracts.game.address),
    );
    try {
      const result: string[] = await instance.methods
        .listCharacterOf(account, page)
        .call();
      const characters: CharacterInterface[] = [];
      result.forEach((_id, i) => {
        if (i >= 4) {
          return;
        }
        const id = parseInt(_id);
        if (id) {
          characters.push({
            id,
          });
        }
      });
      return characters;
    } catch (e) {
      console.error('error getting listCharacterOf', e);
    }
  }

  async listTargetOfCharacter(tokenId: number) {
    const web3 = (window as any).web3;
    const Contract = web3.eth.Contract;
    const instance = new Contract(
      contracts.game.abi,
      getAddress(contracts.game.address),
    );
    try {
      const result = await instance.methods
        .listTargetOfCharacter(tokenId)
        .call();
      const challengers: ChallengerInterface[] = [];
      result.forEach((e, i) => {
        if (i >= 4) {
          return;
        }
        challengers.push({
          id: parseInt(e.targetId),
          power: parseInt(e.targetId),
          expMax: parseInt(e.expMax),
          expMin: parseInt(e.expMin),
          winRate: parseInt(e.winRate),
          rewardMin: new BigNumber(e.rewardMin),
          rewardMax: new BigNumber(e.rewardMax),
        });
      });
      return challengers;
    } catch (e) {
      console.error('error getting listTargetOfCharacter', e);
    }
  }
}

export const gameService = new GameService();
