import { selectApp } from 'app/slice/selectors';
import React, { useEffect } from 'react';
import { Button } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { formatAmountNumber, formatWalletAddress } from 'utils/wallet';
import { useWallet } from 'hooks/useWallet';
import { getExchangeLink } from 'utils/linkHelpers';
import { getDballAddress, getChain } from 'utils/addressHelpers';
import { ReactComponent as PancakeLogo } from 'assets/images/pancake.svg';

export const Wallet: React.FC = () => {
  const { wallet } = useSelector(selectApp);
  const { connectWallet } = useWallet();

  const exchangeLink = getExchangeLink(getDballAddress(), getChain());
  useEffect(() => {
    (async () => {
      const account = await connectWallet();
      console.log('Connected as', account);
    })();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  let WalletAmount;
  if (wallet.balance) {
    WalletAmount = (
      <div>
        Balance: {formatAmountNumber(wallet.balance, 4)}{' '}
        <a href={exchangeLink} target="_blank" rel="noreferrer">
          DBALL
        </a>{' '}
        <a href={exchangeLink} target="_blank" rel="noreferrer">
          <PancakeLogo />
        </a>
      </div>
    );
  }

  return (
    <div>
      <div style={{ textAlign: 'left' }}>
        {wallet.account ? (
          'Account: ' + formatWalletAddress(wallet.account)
        ) : (
          <Button
            onClick={connectWallet}
            className="btn-grad-primary"
            style={{ fontWeight: 'bold' }}
          >
            Connect MetaMask
          </Button>
        )}
        {WalletAmount}
      </div>
    </div>
  );
};
