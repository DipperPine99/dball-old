/**
 *
 * StaminaCompensation
 *
 */
import { useAppSlice } from 'app/slice';
import { selectApp } from 'app/slice/selectors';
import { useCallback, useEffect } from 'react';
import { Card, Col, Container, Row } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { compensationService } from 'services/smartcontracts/CompensationService';
import { fetchOwnedCharacters } from 'utils/updateCharacters';

interface Props {}

export function StaminaCompensation(props: Props) {
  const { wallet, compensation } = useSelector(selectApp);
  const { actions } = useAppSlice();
  const dispatch = useDispatch();

  const refreshStaminaCompensation = useCallback(async () => {
    if (wallet.account) {
      const compensation = await compensationService.staminaCompensationCount(
        wallet.account,
      );
      dispatch(actions.setCompensation(compensation));
    }
  }, [actions, wallet.account, dispatch]);

  const redeemStaminaCompensation = useCallback(async () => {
    if (wallet.account) {
      await compensationService.claimStamina(wallet.account);
      refreshStaminaCompensation();
      fetchOwnedCharacters(wallet.account);
    }
  }, [refreshStaminaCompensation, wallet.account]);

  useEffect(() => {
    (async () => {
      await refreshStaminaCompensation();
    })();
  }, [refreshStaminaCompensation]);

  if (compensation && compensation.remaining > 0) {
    let GetStaminaBlock;
    if (compensation.nextClaim > Date.now() / 1000) {
      let nextClaimDate = new Date(compensation.nextClaim * 1000);
      let nextDate = nextClaimDate.getDate().toString().padStart(2, '0');
      let nextMonth = (nextClaimDate.getMonth() + 1)
        .toString()
        .padStart(2, '0');
      let nextYear = nextClaimDate.getFullYear();
      let nextHours = nextClaimDate.getHours().toString().padStart(2, '0');
      let nextMinutes = nextClaimDate.getMinutes().toString().padStart(2, '0');
      GetStaminaBlock = (
        <Card.Text>
          Wait until {nextYear}/{nextMonth}/{nextDate} {nextHours}:{nextMinutes}{' '}
          to get your stamina
        </Card.Text>
      );
    } else {
      GetStaminaBlock = (
        <Card.Link
          href="#"
          onClick={e => {
            e.preventDefault();
            redeemStaminaCompensation();
          }}
        >
          Get our stamina compensation - {compensation.remaining} left
        </Card.Link>
      );
    }
    return (
      <Container className="pt-5">
        <Row>
          <Col>
            <Card>
              <Card.Body>
                <Card.Title>Stamina compensation</Card.Title>
                {/* <Card.Subtitle className="mb-2 text-muted">
                  Card Subtitle
                </Card.Subtitle> */}
                <Card.Text>
                  As the impact due to system upgrade on August 10th, please
                  click below link to get compensation from CryptoDrakeBall
                  team.
                  <br></br>
                  Total stamina compensated: 24<br></br>
                  Bonus stamina is divided into {compensation.maxClaim} times to
                  be claimed for your impacted warriors with 8 stamina per 24
                  getHours<br></br>
                </Card.Text>
                {GetStaminaBlock}
                <Card.Text className="pt-4">
                  <b>Note:</b>
                  <br></br>2 types of Stamina:<br></br>+ Normal Stamina - Get
                  EXP plus immediately<br></br>+ Bonus Stamina - Stacked EXP,
                  plus after you perform a battle with normal stamina<br></br>
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    );
  } else {
    return <></>;
  }
}
