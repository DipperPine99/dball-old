import Slider from 'react-slick';
import styled from 'styled-components';
import ArrowLeft from 'assets/images/left-button.svg';
import ArrowRight from 'assets/images/right-button.svg';
import { FC } from 'react';
import { CharacterItem } from './CharacterItem';
import { CharacterModel } from 'models/CharacterModel';
// import characterData from "../../assets/data-character.json";
import _ from 'lodash';
import { plainToClass } from 'class-transformer';
const StyledArrow = styled.img`
  width: 24px;
  height: 44px;
  transform: translateY(-80px);
  z-index: 1;
  @media (max-width: 576px) {
    &:first-child {
      left: -10px;
    }
  }
`;

const StyledSlider = styled.div`
  @media (max-width: 576px) {
    .slick-next {
      right: -10px !important;
    }
  }
`;

interface Props {
  data: any;
}

const SliderCardItem: FC<Props> = ({ data }) => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: <StyledArrow src={ArrowRight} />,
    prevArrow: <StyledArrow src={ArrowLeft} />,
  };
  return (
    <StyledSlider>
      <Slider {...settings}>
        {data?.map((char, index) => (
          <CharacterItem
            key={char.id}
            char={plainToClass(CharacterModel, char)}
          />
        ))}
      </Slider>
    </StyledSlider>
  );
};

export default SliderCardItem;
