import { CharacterItemProps } from 'utils/types';
import React, { useCallback } from 'react';
import { useAppSlice } from 'app/slice';
import { useDispatch, useSelector } from 'react-redux';
import { selectApp } from 'app/slice/selectors';
import { gameService } from 'services/smartcontracts/GameService';
import { fetchAllowance, fetchCharacterInfo } from 'utils/updateCharacters';
import { Button, Card, Col } from 'react-bootstrap';
import { tokenService } from 'services/smartcontracts/TokenService';
import { toast } from 'react-toastify';
import styles from './CharacterItem.module.css';
import styled from 'styled-components';
import buyButton from 'assets/images/button2.svg';
import CardItem from '../Common/CardItem';

interface ActionProps {
  padding?: String;
  background?: String;
  disabled?: boolean;
}
export const CharacterItem: React.FC<CharacterItemProps> = ({ char }) => {
  const { actions } = useAppSlice();
  const dispatch = useDispatch();
  const { selectedCharacter, wallet } = useSelector(selectApp);

  const onSelect = useCallback(() => {
    dispatch(actions.setSelectedCharacterId(char.id));
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const onHatch = useCallback(async () => {
    try {
      await gameService.hatch(char.id, txHash => {
        toast("Threw capsule, it's opening...", {
          // position: 'top-right',
          // autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      });
      toast.success("Congrats, you've got new hero", {
        // position: 'top-right',
        // autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      fetchCharacterInfo(char.id);
    } catch (e) {
      if (e.code === 4001) {
        toast.warning('Cancelled throwing capsule', {
          // position: 'top-right',
          // autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      } else {
        toast.error('Threw capsule unsuccessfully, please try again', {
          // position: 'top-right',
          // autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    }
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const approve = useCallback(async () => {
    try {
      await tokenService.approve(txHash => {
        toast('Approving to use DBALL', {
          // position: 'top-right',
          // autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      });
      toast.success("Approved successfully , let's play", {
        // position: 'top-right',
        // autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });

      await fetchAllowance();
    } catch (e) {
      if (e.code === 4001) {
        toast.warning('Cancelled approval', {
          // position: 'top-right',
          // autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      } else {
        toast.error('Approve failed, please try again', {
          // position: 'top-right',
          // autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    }
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const isSelected = selectedCharacter.id === char.id;
  const isApproved =
    wallet.allowance && wallet.allowance >= 10000000000000000000000;

  let ActionButton;
  let actionText;
  let disabled = false;
  let status;
  let buttonStyle;
  if (isApproved) {
    if (char.hatched) {
      if (
        char.stamina !== undefined &&
        char.stamina < 3 &&
        (char.extraStamina || 0) < 3
      ) {
        actionText = ' Wait 1 hour to increase stamina';
        status = 'waiting';
        disabled = true;
      } else {
        if (isSelected) {
          disabled = true;
          actionText = 'Selected';
        } else {
          ActionButton = onSelect;
          actionText = 'Select';
        }
      }
    } else {
      ActionButton = onHatch;
      actionText = 'Throw (0.9999 DBALL)';
      buttonStyle = { fontSize: '10px' };
    }
  } else {
    ActionButton = approve;
    actionText = 'Approve';
  }
  console.log(selectedCharacter);
  return (
    <>
      {char.getName() && (
        <CardItem
          type="character"
          status={status}
          nameItem={char.getName()}
          backgroundUrl={char.getImageUrl()}
          actionText={actionText}
          actionButton={ActionButton}
          disabled={disabled}
          selectedCharacterId={selectedCharacter.id}
          buttonStyle={buttonStyle}
          {...char}
        ></CardItem>
      )}
    </>
  );
};
