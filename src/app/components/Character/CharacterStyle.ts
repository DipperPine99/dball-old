import styled from 'styled-components';

export const StyledSelectText = styled.div`
  font-style: normal;
  font-weight: normal;
  font-size: 13px;
  line-height: 16px;
  text-align: center;
  color: #f29521;
`;

export const StyleCapsule = styled.div`
  display: flex;
  align-items: center;
  background-repeat: no-repeat !important;
  img {
    z-index: 1;
  }
  margin-top: 2rem;
  margin-bottom: 4rem;
  min-height: 62px;
  &:hover {
    cursor: pointer;
  }
`;
