import React from 'react';
import { CharacterItem } from './CharacterItem';
import { useSelector } from 'react-redux';
import { selectApp } from 'app/slice/selectors';
import { CharacterModel } from 'models/CharacterModel';
import { plainToClass } from 'class-transformer';
import { Col, Row } from 'react-bootstrap';
import SliderCardItem from './SliderCardItem';

export const CharacterList: React.FC = () => {
  const { ownedCharacters: characters } = useSelector(selectApp);
  return (
    <>
      <Col
        xs="12"
        md="12"
        className="d-lg-block d-none"
        style={{ padding: '0' }}
      >
        <Row>
          <Col xs="6" style={{ padding: '0 10px' }}>
            {characters?.map((char, index) => {
              if (index % 2 == 0)
                return (
                  <CharacterItem
                    key={char.id}
                    char={plainToClass(CharacterModel, char)}
                  />
                );
            })}
          </Col>
          <Col xs="6" md="6" style={{ marginTop: '5rem', padding: '0 10px' }}>
            {characters?.map((char, index) => {
              if (index % 2 !== 0)
                return (
                  <CharacterItem
                    key={char.id}
                    char={plainToClass(CharacterModel, char)}
                  />
                );
            })}
          </Col>
        </Row>
      </Col>

      {characters?.map((char, index) => (
        <Col
          className="d-none d-md-block d-lg-none"
          xs="12"
          md="6"
          lg="4"
          xl="3"
        >
          <CharacterItem
            key={char.id}
            char={plainToClass(CharacterModel, char)}
          />
        </Col>
      ))}
      <Col className="d-md-none">
        <SliderCardItem data={characters} />
      </Col>
    </>
  );
};
