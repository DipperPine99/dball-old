import { FC, CSSProperties, useState, useEffect } from 'react';
import styled from 'styled-components';
// import ImageConst from "../../constants/image";
// import { StyledAction } from "./index";
// import { ImageType } from "../../constants/image";
import DBallIcon from 'assets/images/dball-icon.svg';
import ArrowLeft from 'assets/images/left-button.svg';
import ArrowRight from 'assets/images/right-button.svg';
import buyButton from 'assets/images/button2.svg';
import cardBack from 'assets/images/card-back.png';
import { formatAmount } from 'utils/wallet';
import { BigNumber } from 'bignumber.js';
import { useSpring, a, animated } from '@react-spring/web';
import { ChallengerItemProps } from 'utils/types';

interface ActionProps {
  padding?: String;
  background?: String;
  disabled?: boolean;
}

interface Props {
  nameItem?: String;
  id?: number;
  level?: Number;
  expMin?: Number;
  expMax?: Number;
  exp?: Number;
  rare?: Number;
  stamina?: Number;
  status?: String;
  type?: String;
  winRate?: Number;
  rewardMin?: BigNumber;
  rewardMax?: BigNumber;
  expGain?: String;
  backgroundUrl?: string;
  actionText?: String;
  showInfo?: Boolean;
  additionButton?: String;
  buttonStyle?: CSSProperties;
  price?: number;
  showArrow?: Boolean;
  hasArrow?: Boolean;
  hatched?: boolean;
  actionFlipBg?: () => void;
  actionButton?: () => Promise<void>;
  disabled?: boolean;
  selectedCharacterId?: number;
  challengerDefault?: ChallengerItemProps;
}

const StyledAction = styled.div.attrs((props: ActionProps) => ({
  padding: props.padding,
  background: props.background || buyButton,
  disabled: props.disabled,
}))`
  font-style: normal;
  font-weight: normal;
  font-size: 13px;
  line-height: 16px;
  text-align: center;
  background: ${(props: ActionProps) => `url('${props.background}')`};
  box-sizing: border-box;
  color: #f29521;
  height: 37px;
  background-size: contain;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 115px;
  &:hover {
    cursor: ${(props: ActionProps) => (props.disabled ? 'no-drop' : 'pointer')};
  }
`;

const StyledWrapper = styled.div`
  margin-bottom: 60px;
`;
const StyledCard = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  background-size: cover;
  position: relative;
  @media (max-width: 768px) and (min-width: 576px) {
    width: fit-content;
    margin: auto;
  }
  @media (max-width: 576px) {
    width: fit-content;
    margin: auto;
  }
`;
const StyledName = styled.div`
  font-style: normal;
  font-weight: 700;
  font-size: 13px;
  line-height: 15px;
  color: #fff;
  margin-bottom: 10px;
  @media (max-width: 1400px) and (min-width: 1200px) {
    font-size: 11px;
  }
  @media (max-width: 1200px) and (min-width: 992px) {
    font-size: 10px;
    margin-bottom: 5px;
  }
  @media (max-width: 992px) and (min-width: 768px) {
    font-size: 14px;
  }
  @media (max-width: 768px) and (min-width: 576px) {
    font-size: 16px;
  }
  @media (max-width: 576px) and (min-width: 414px) {
    font-size: 14px;
  }
  @media (max-width: 414px) {
    font-size: 14px;
  }
`;

const StyledNameEnemy = styled.div`
  font-style: normal;
  font-weight: bold;
  font-size: 13px;
  line-height: 1.2 !important;
  color: #fff;
  margin-bottom: 1em;
  @media (max-width: 1400px) and (min-width: 1200px) {
    font-size: 11px;
  }
  @media (max-width: 1200px) and (min-width: 992px) {
    font-size: 10px;
    margin-bottom: 5px;
  }
  @media (max-width: 992px) and (min-width: 768px) {
    font-size: 14px;
  }
  @media (max-width: 768px) and (min-width: 576px) {
    font-size: 15px;
  }
  @media (max-width: 576px) and (min-width: 414px) {
    font-size: 14px;
  }
  @media (max-width: 414px) and (min-width: 375px) {
    font-size: 14px;
  }
  @media (max-width: 375px) {
    font-size: 12px;
  }
`;

const StyledInfo = styled.div`
  color: #fff;
`;

const StyledText = styled.p`
  line-height: 1.3 !important;
  font-style: normal;
  font-weight: 400;
  font-size: 13px;
  line-height: 1.2 !important;
  margin-bottom: 0px;
  @media (max-width: 1400px) and (min-width: 1200px) {
    font-size: 11px;
  }
  @media (max-width: 1200px) and (min-width: 992px) {
    font-size: 10px;
  }
  @media (max-width: 992px) and (min-width: 768px) {
    font-size: 14px;
  }
  @media (max-width: 768px) and (min-width: 576px) {
    font-size: 15px;
  }
  @media (max-width: 576px) and (min-width: 414px) {
    font-size: 14px;
  }
  @media (max-width: 414px) and (min-width: 375px) {
    font-size: 14px;
  }
  @media (max-width: 375px) {
    font-size: 12px;
  }
`;
const StyledStatus = styled.div`
  font-weight: bold;
  font-size: 10px;
  line-height: 1.2 !important;
  display: flex;
  align-items: center;
  margin-top: 10px;
  img {
    margin-left: 5px;
  }
  @media (max-width: 1400px) and (min-width: 1200px) {
    font-size: 9px;
  }
  @media (max-width: 1200px) and (min-width: 992px) {
    margin-top: 6px;
    font-size: 7px;
  }
  @media (max-width: 992px) and (min-width: 768px) {
    font-size: 10px;
  }
  @media (max-width: 768px) and (min-width: 414px) {
    font-size: 11px;
  }
  @media (max-width: 414px) and (min-width: 375px) {
    font-size: 11px;
  }
  @media (max-width: 375px) {
    font-size: 10px;
  }
`;

const StyledBackground = styled.img`
  width: 100%;
  height: 100%;

  @media (max-width: 576px) {
    padding: 0 1.5em;
  }
`;
const StyledBackgroundCover = styled.img`
  width: 100%;
  height: 100%;

  position: absolute;
  @media (max-width: 576px) {
    padding: 0 1.5em;
  }
`;

const StyledInfoWrapper = styled.div`
  height: 100%;
  display: flex;
  /* align-items: center; */
  padding-top: 40px;
  width: 100%;
  padding-left: 50%;
  position: absolute;
  padding-right: 15px;
  @media (max-width: 1400px) and (min-width: 1200px) {
    padding-top: 30px;
  }
  @media (max-width: 1200px) and (min-width: 992px) {
    padding-top: 24px;
  }
  @media (max-width: 767px) and (min-width: 576px) {
    padding-top: 50px;
  }
  @media (max-width: 576px) and (min-width: 414px) {
    padding-right: 40px;
    padding-top: 50px;
  }
  @media (max-width: 414px) and (min-width: 375px) {
    padding-top: 35px;
    padding-right: 25px;
  }
  @media (max-width: 375px) {
    padding-top: 40px;
  }
`;

const CardItem: FC<Props> = props => {
  const [flipped, setFlipped] = useState(false);
  const { transform, opacity } = useSpring({
    opacity: !props.selectedCharacterId ? 1 : 0,
    transform: `perspective(600px) rotateY(${
      flipped ? (props.type === 'challenger' ? 360 : 180) : 0
    }deg)`,
    config: { mass: 5, tension: 500, friction: 80 },
  });
  useEffect(() => {
    if (props.selectedCharacterId && props.type === 'challenger') {
      setFlipped(true);
    } else {
      setFlipped(false);
    }
  }, [props.selectedCharacterId]);

  const handleAction = (e: React.MouseEvent) => {
    if (props.actionButton) {
      props.actionButton();
    }
    e.stopPropagation();
  };

  return (
    <StyledWrapper>
      <StyledCard>
        <div className="d-flex justify-content-center align-items-center">
          <StyledBackground
            as={animated.img}
            src={props.backgroundUrl}
            alt="Background"
            style={{
              opacity: opacity.to(o => 1 - o) as any,
              transform: transform as any,
            }}
          />
          <StyledBackgroundCover
            src={
              props.type === 'challenger' && !props.selectedCharacterId
                ? cardBack
                : props.backgroundUrl
            }
            as={animated.img}
            alt="Background"
            style={{
              opacity: opacity as any,
              transform: transform as any,
            }}
          />
        </div>

        <StyledInfoWrapper
          as={animated.div}
          style={{
            opacity:
              props.type === 'challenger' && !props.selectedCharacterId
                ? 0
                : (1 as any),
            transform: transform as any,
          }}
        >
          {props.hatched && props.type === 'character' && (
            <div>
              <StyledNameEnemy>{props.nameItem}</StyledNameEnemy>
              <StyledInfo>
                <StyledText>Win Rate: {props.winRate}%</StyledText>
                <StyledText>
                  Reward: {props.rewardMin ? formatAmount(props.rewardMin) : ''}{' '}
                  - {props.rewardMax ? formatAmount(props.rewardMax) : ''} DBALL
                </StyledText>
                <StyledText>
                  Exp Gain: {props.expMin ? props.expMin : ''} -{' '}
                  {props.expMax ? props.expMax : ''} exp
                </StyledText>
              </StyledInfo>
            </div>
          )}
          {props.type === 'challenger' && (
            <div>
              <StyledNameEnemy>{props.nameItem}</StyledNameEnemy>
              <StyledInfo>
                <StyledText>Win Rate: {props.winRate}%</StyledText>
                <StyledText>
                  Reward: {props.rewardMin ? formatAmount(props.rewardMin) : ''}{' '}
                  - {props.rewardMax ? formatAmount(props.rewardMax) : ''} DBALL
                </StyledText>
                <StyledText>
                  Exp Gain: {props.expMin ? props.expMin : ''} -{' '}
                  {props.expMax ? props.expMax : ''} exp
                </StyledText>
              </StyledInfo>
            </div>
          )}
        </StyledInfoWrapper>
      </StyledCard>
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          marginTop: '1rem',
        }}
      >
        <StyledAction
          style={props.buttonStyle}
          onClick={e => handleAction(e)}
          disabled={props.disabled}
        >
          {props.status ? 'Wait' : props.actionText}
        </StyledAction>
      </div>
    </StyledWrapper>
  );
};

export default CardItem;
