import { ChallengerItemProps } from 'utils/types';
import React, { useCallback, useEffect, useState } from 'react';
import { gameService } from 'services/smartcontracts/GameService';
import { useSelector } from 'react-redux';
import { selectApp } from 'app/slice/selectors';
import { fetchCharacterInfo } from 'utils/updateCharacters';
import styles from './ChallengerItem.module.css';
import { toast } from 'react-toastify';
import CardItem from '../Common/CardItem';

export const ChallengerItem: React.FC<ChallengerItemProps> = ({
  challenger,
}) => {
  const { selectedCharacter, ownedCharacters } = useSelector(selectApp);
  const [isNotEnoughStamina, setIsNotEnoughStamina] = useState(false);
  const { ownedCharacters: characters } = useSelector(selectApp);

  const fight = useCallback(async id => {
    if (id && challenger.power) {
      try {
        const result = await gameService.combat(
          id,
          challenger.power,
          txHash => {
            toast('Fighting', {
              position: 'top-center',
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          },
        );
        if (result) {
          fetchCharacterInfo(id);
          const formattedToken = (window as any).web3.utils.fromWei(
            result.ballGain.toJSON(),
            'ether',
          );
          if (result.isWin) {
            toast.success(
              'You won, gained ' +
                result.xpGain +
                ' exp and ' +
                formattedToken +
                ' DBALL',
              {
                position: 'top-center',
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
              },
            );
          } else {
            toast('You lose, gained ' + result.xpGain + ' exp', {
              position: 'top-center',
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          }
        }
      } catch (e) {
        if (e.code === 4001) {
          toast.warning('Cancelled fighting', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        } else {
          toast.error('Error in fighting, please check again', {
            position: 'top-center',
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
      }
    }
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    ownedCharacters?.forEach(e => {
      if (
        e.stamina !== undefined &&
        e.stamina < 3 &&
        (e.extraStamina || 0) < 3 &&
        selectedCharacter.id === e.id
      ) {
        setIsNotEnoughStamina(true);
        return;
      }
    });
  }, [selectedCharacter.id, ownedCharacters, setIsNotEnoughStamina]);

  let actionText;
  let disabled = false;
  let buttonStyle;
  if (isNotEnoughStamina) {
    actionText = 'Choose a hero to fight';
    disabled = true;
    buttonStyle = { fontSize: '10px' };
  } else if (selectedCharacter.id) {
    actionText = 'Fight';
  }
  return (
    <>
      {characters && (
        <CardItem
          type="challenger"
          nameItem={challenger.getName()}
          backgroundUrl={challenger.getImageUrl()}
          actionText={actionText}
          actionButton={() => fight(selectedCharacter.id)}
          disabled={disabled}
          selectedCharacterId={selectedCharacter.id}
          buttonStyle={buttonStyle}
          {...challenger}
        ></CardItem>
      )}
    </>
  );
};
