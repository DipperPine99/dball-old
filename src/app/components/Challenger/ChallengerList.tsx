import { ChallengerInterface } from 'utils/types';
import React, { useEffect, useState } from 'react';
import { ChallengerItem } from './ChallengerItem';
import { selectApp } from 'app/slice/selectors';
import { useSelector } from 'react-redux';
import { Col, Row } from 'react-bootstrap';
import { plainToClass } from 'class-transformer';
import { ChallengerModel } from 'models/ChallengerModel';
import { range, shuffle } from 'lodash';
import { AVAILABLE_VILLAIN_COUNT } from 'config/constants';
import { gameService } from 'services/smartcontracts/GameService';

const defaultChallengers = [
  {
    classId: 5,
    expMax: 420,
    expMin: 300,
    id: 1,
    power: 2,
    winRate: 70,
  },
  {
    classId: 4,
    expMax: 1080,
    expMin: 600,
    id: 2,
    power: 4,
    winRate: 30,
  },
  {
    classId: 2,
    expMax: 300,
    expMin: 285,
    id: 3,
    power: 1,
    winRate: 90,
  },
  {
    classId: 3,
    expMax: 600,
    expMin: 420,
    id: 4,
    power: 3,
    winRate: 50,
  },
];
export const ChallengerList: React.FC = () => {
  const { selectedCharacter } = useSelector(selectApp);
  const [challengers, setChallengers] = useState<Array<ChallengerInterface>>(
    [],
  );
  const [isOpenChallenger, setIsOpenChallenger] = useState(false);

  const isSelected = selectedCharacter.id ? true : false;
  useEffect(() => {
    (async () => {
      if (selectedCharacter.id) {
        const challengers = await gameService.listTargetOfCharacter(
          selectedCharacter.id,
        );
        setChallengers([]); // reset challengers - there is an issue with replacing id
        if (challengers) {
          const shuffledChallengers: ChallengerInterface[] =
            shuffle(challengers);
          const villains: number[] = shuffle(
            range(1, AVAILABLE_VILLAIN_COUNT + 1),
          );
          shuffledChallengers.map((e, i) => {
            e.id = i + 1;
            e.classId = villains[i];
            return e;
          });
          setIsOpenChallenger(true);
          setChallengers(shuffledChallengers);
        }
      } else {
        setChallengers(defaultChallengers);
      }
    })();
  }, [selectedCharacter.id]);

  return (
    <>
      <Col
        xs="12"
        md="12"
        className="d-lg-block d-none"
        style={{ padding: '0' }}
      >
        <Row>
          <Col xs="6" style={{ marginTop: '5rem', padding: '0 10px' }}>
            {challengers.map((item, index) => {
              if (index % 2 == 0)
                return (
                  <ChallengerItem
                    key={item.id}
                    challenger={plainToClass(ChallengerModel, item)}
                  />
                );
            })}
          </Col>
          <Col xs="6" md="6" style={{ padding: '0 10px' }}>
            {challengers.map((item, index) => {
              if (index % 2 != 0)
                return (
                  <ChallengerItem
                    key={item.id}
                    challenger={plainToClass(ChallengerModel, item)}
                  />
                );
            })}
          </Col>
        </Row>
      </Col>
      {isSelected
        ? challengers?.map(item => (
            <Col
              className="d-none d-md-block d-lg-none"
              xs="12"
              md="6"
              lg="4"
              xl="3"
            >
              <ChallengerItem
                key={item.id}
                challenger={plainToClass(ChallengerModel, item)}
              />
            </Col>
          ))
        : ''}
      {isSelected
        ? challengers?.map(item => (
            <Col className="d-lg-none" xs="12" md="6" lg="4" xl="3">
              <ChallengerItem
                key={item.id}
                challenger={plainToClass(ChallengerModel, item)}
              />
            </Col>
          ))
        : ''}
    </>
  );
};
