import { CharacterInterface } from 'utils/types';

/* --- STATE --- */
export interface AppState {
  // web3?: any;
  wallet: WalletState;
  compensation?: StaminaCompensationState;
  selectedCharacter: {
    id?: number;
  };
  ownedCharacters?: CharacterInterface[];
}

export interface WalletState {
  connected: boolean;
  account?: string;
  networkId?: number;
  balance?: number;
  allowance?: number;
}

export interface StaminaCompensationState {
  isClaimable: boolean;
  remaining: number;
  maxClaim: number;
  claimed: number;
  nextClaim: number;
}
