import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { CharacterInterface } from 'utils/types';
import { appSaga } from './saga';
import { AppState, StaminaCompensationState, WalletState } from './types';

export const initialState: AppState = {
  wallet: {
    connected: false,
    account: undefined,
    networkId: undefined,
  },
  selectedCharacter: {},
};

const slice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    setWallet(state, action: PayloadAction<WalletState>) {
      state.wallet = action.payload;
    },
    setCompensation(state, action: PayloadAction<StaminaCompensationState>) {
      state.compensation = action.payload;
    },
    setSelectedCharacterId(state, action: PayloadAction<number>) {
      state.selectedCharacter.id = action.payload;
    },
    setOwnedCharacters(state, action: PayloadAction<CharacterInterface[]>) {
      state.ownedCharacters = action.payload;
    },
    setWalletBalance(state, action: PayloadAction<number>) {
      state.wallet.balance = action.payload;
    },
    setWalletAllowance(state, action: PayloadAction<number>) {
      state.wallet.allowance = action.payload;
    },
    updateCharacterInfo(
      state,
      action: PayloadAction<{ id: number; info: any }>,
    ) {
      if (!state.ownedCharacters) return;
      const index = state.ownedCharacters?.findIndex(
        e => e.id === action.payload.id,
      ); // finding index of the item
      const newArray = [...state.ownedCharacters]; // making a new array
      newArray[index] = { ...newArray[index], ...action.payload.info }; //changing value in the new array
      state.ownedCharacters = newArray;
    },
  },
});

export const { actions: appActions } = slice;

export const useAppSlice = () => {
  useInjectReducer({ key: slice.name, reducer: slice.reducer });
  useInjectSaga({ key: slice.name, saga: appSaga });
  return { actions: slice.actions };
};

/**
 * Example Usage:
 *
 * export function MyComponentNeedingThisSlice() {
 *  const { actions } = useAppSlice();
 *
 *  const onButtonClick = (evt) => {
 *    dispatch(actions.someAction());
 *   };
 * }
 */
