import { CharacterList } from 'app/components/Character/CharacterList';
import { ChallengerList } from 'app/components/Challenger/ChallengerList';
import { Helmet } from 'react-helmet-async';
import { Container, Row, Col, Button, Tooltip } from 'react-bootstrap';
import menuImage from 'assets/images/menu.png';
import logoImage from 'assets/images/logo.svg';
import subLogoImage from 'assets/images/sub-logo.svg';
import logoImageMobile from 'assets/images/logo-mobile.png';
import subLogoImageMobile from 'assets/images/sub-logo-mobile.png';
import buttonCapsule from 'assets/images/Button/buy-button.png';
import { Wallet } from 'app/components/Header/Wallet';
import { useSelector } from 'react-redux';
import { selectApp } from 'app/slice/selectors';
import { useWallet } from 'hooks/useWallet';
import { useCallback, useEffect, useState } from 'react';
import { toast, ToastContainer } from 'react-toastify';
import { StaminaCompensation } from 'app/components/Compensation/StaminaCompensation';
import { useHistory } from 'react-router-dom';
import {
  StyledAccount,
  StyledLogo,
  StyledLogoHidden,
  StyledLogoMobile,
  StyledMainLogo,
  StyledMainLogoMobile,
  StyledMenu,
  StyledSubLogo,
  StyledSubLogoMobile,
  StyledTitle,
  StyledTitleLeft,
  StyledTitleMobile,
  StyledTitleRight,
  StyledWrapper,
  StyledWrapperMobile,
  StyleMemuIcon,
} from './indexStyle';
import {
  StyleCapsule,
  StyledSelectText,
} from '../../components/Character/CharacterStyle';
import { migrateNFTService } from 'services/smartcontracts/MigrateNFTService';
import { fetchAllowance, fetchOwnedCharacters } from 'utils/updateCharacters';
import { gameService } from 'services/smartcontracts/GameService';
import { tokenService } from 'services/smartcontracts/TokenService';

export function HomePage() {
  const history = useHistory();
  const { ownedCharacters: characters, wallet } = useSelector(selectApp);
  const [needMigrate, setNeedMigrate] = useState(false);
  const [openMenu, setOpenMenu] = useState(false);
  const { connectWallet } = useWallet();

  useEffect(() => {
    refreshCharacters();
    checkMigrate();
  }, [wallet.account]); // eslint-disable-line react-hooks/exhaustive-deps
  const handleRedirect = (to: string) => {
    setOpenMenu(false);
    history.push(to);
  };
  const onConnectWallet = useCallback(() => {
    connectWallet();
  }, [connectWallet]);

  const renderTooltip = props => (
    <Tooltip id="button-tooltip" {...props}>
      Coming soon
    </Tooltip>
  );
  const create = useCallback(async () => {
    try {
      await gameService.evolve(txHash => {
        toast('Capsule is creating', {
          // position: 'top-right',
          // autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      });
      toast.success('Capsule is created', {
        // position: 'top-right',
        // autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      if (wallet.account) {
        await fetchOwnedCharacters(wallet.account);
      }
    } catch (e) {
      if (e.code === 4001) {
        toast.warning('Cancelled creating capsule', {
          // position: 'top-right',
          // autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      } else {
        toast.error('Error on creating new capsule, please try again', {
          // position: 'top-right',
          // autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    }
  }, [wallet.account]); // eslint-disable-line react-hooks/exhaustive-deps

  const migrate = useCallback(async () => {
    if (wallet.account) {
      const migrateStatus = await migrateNFTService.migrateNFT(wallet.account);
      console.log('migrateStatus', migrateStatus);
      await fetchOwnedCharacters(wallet.account);
    }
  }, [wallet.account]); // eslint-disable-line react-hooks/exhaustive-deps

  const refreshCharacters = useCallback(async () => {
    if (wallet.account) {
      await fetchOwnedCharacters(wallet.account);
    }
  }, [wallet.account]);

  const checkMigrate = useCallback(async () => {
    if (wallet.account) {
      const needMigrate = await migrateNFTService.isNeededToMigrate(
        wallet.account,
      );
      if (needMigrate) {
        setNeedMigrate(needMigrate);
      }
    }
  }, [wallet.account]);

  const approve = useCallback(async () => {
    await tokenService.approve();
    await fetchAllowance();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const isApproved =
    wallet.allowance && wallet.allowance >= 10000000000000000000000;
  let CreateCharacter;
  if (isApproved) {
    if (needMigrate && characters && characters.length === 0) {
      CreateCharacter = (
        <Col xs="12" md={12}>
          <StyleCapsule style={{ background: `url(${buttonCapsule})` }}>
            <StyledSelectText onClick={migrate}>
              Recover your heroes
            </StyledSelectText>
          </StyleCapsule>
        </Col>
      );
    } else {
      if (!characters?.length || characters.length < 4) {
        CreateCharacter = (
          <Col xs="12" md={12}>
            <StyleCapsule style={{ background: `url(${buttonCapsule})` }}>
              <StyledSelectText
                onClick={create}
                style={{ transform: 'translate(40px , 3px)' }}
              >
                New capsule (19.9186 DBALL)
              </StyledSelectText>
            </StyleCapsule>
          </Col>
        );
      }
    }
  } else {
    CreateCharacter = (
      <Col xs="12" md={12}>
        <StyleCapsule style={{ background: `url(${buttonCapsule})` }}>
          <StyledSelectText
            style={{ transform: 'translate(50px , 3px)' }}
            onClick={approve}
          >
            Approve
          </StyledSelectText>
        </StyleCapsule>
      </Col>
    );
  }
  let ContentBlock;
  if (wallet.account) {
    ContentBlock = (
      <>
        <Row className="d-block">{CreateCharacter}</Row>
        <Row>
          <Col xs="6" md="6" className="d-lg-block d-none">
            <CharacterList></CharacterList>
            <StaminaCompensation />
          </Col>
          <Col xs="6" md="6" className="d-lg-block d-none">
            <ChallengerList></ChallengerList>
          </Col>
        </Row>
        <Row className=" d-md-flex d-lg-none">
          <CharacterList></CharacterList>
          <StaminaCompensation />
        </Row>
        <Row className=" d-md-flex d-lg-none">
          <ChallengerList></ChallengerList>
        </Row>
      </>
    );
  } else {
    ContentBlock = (
      <Container>
        <Row>
          <Col xs="12" md={12}>
            <StyleCapsule style={{ background: `url(${buttonCapsule})` }}>
              <StyledSelectText
                style={{ transform: 'translate(50px , 3px)' }}
                onClick={onConnectWallet}
              >
                Connect your wallet to continue
              </StyledSelectText>
            </StyleCapsule>
          </Col>
        </Row>
      </Container>
    );
  }
  return (
    <>
      <Helmet>
        <title>Play</title>
        <meta
          name="description"
          content="Play to earn DBALL today, amazing NFT game"
        />
      </Helmet>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
      <StyledWrapper className="d-none d-lg-flex justify-content-space-around">
        <StyledTitleLeft>
          <StyledTitle onClick={() => handleRedirect('#arena')}>
            Arena
          </StyledTitle>
          <StyledTitle onClick={() => handleRedirect('#marketplace')}>
            Marketplace
          </StyledTitle>
          <StyledTitle onClick={() => handleRedirect('#roomofspirit')}>
            Room of spirit
          </StyledTitle>
        </StyledTitleLeft>
        <StyledLogoHidden className="invisible"></StyledLogoHidden>
        <StyledLogo>
          <StyledMainLogo src={logoImage} alt="Des" />
          <StyledSubLogo src={subLogoImage} alt="Des" />
        </StyledLogo>
        <StyledTitleRight>
          <StyledTitle onClick={() => handleRedirect('#stake')}>
            Stake
          </StyledTitle>
          <StyledTitle onClick={() => handleRedirect('#farm')}>
            Farm
          </StyledTitle>
          <StyledTitle onClick={() => handleRedirect('#help')}>
            Help
          </StyledTitle>
        </StyledTitleRight>
        <StyledAccount>
          <Wallet></Wallet>
        </StyledAccount>
      </StyledWrapper>
      <StyledWrapperMobile className="d-flex d-lg-none">
        <StyledLogoMobile>
          <StyledMainLogoMobile src={logoImageMobile} alt="Des" />
          <StyledSubLogoMobile src={subLogoImageMobile} alt="Des" />
        </StyledLogoMobile>
        <StyleMemuIcon onClick={() => setOpenMenu(!openMenu)}>
          <img src={menuImage} alt="Icon" style={{ width: '23px' }} />
        </StyleMemuIcon>
      </StyledWrapperMobile>
      {openMenu ? (
        <StyledMenu className="d-flex d-lg-none">
          <StyledTitleMobile onClick={() => handleRedirect('#arena')}>
            Arena
          </StyledTitleMobile>
          <StyledTitleMobile onClick={() => handleRedirect('#marketplace')}>
            Marketplace
          </StyledTitleMobile>
          <StyledTitleMobile onClick={() => handleRedirect('#roomofspirit')}>
            Room of spirit
          </StyledTitleMobile>
          <StyledTitleMobile onClick={() => handleRedirect('#stake')}>
            Stake
          </StyledTitleMobile>
          <StyledTitleMobile onClick={() => handleRedirect('#farm')}>
            Farm
          </StyledTitleMobile>
          <StyledTitleMobile onClick={() => handleRedirect('#help')}>
            Help
          </StyledTitleMobile>
        </StyledMenu>
      ) : null}
      <Container style={{ paddingTop: '100px', paddingBottom: '100px' }}>
        <>{ContentBlock}</>
      </Container>
    </>
  );
}
