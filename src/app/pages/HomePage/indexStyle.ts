import styled from 'styled-components';

export const StyledWrapper = styled.div`
  width: 100%;
  background: #3b1822;
  border-bottom: 2px solid #edcd90;
  border-top: 2px solid #3b1822;
  justify-content: center;
  align-items: center;
  display: flex;
  position: relative;
  height: 56px;
`;

export const StyledTitleLeft = styled.div`
  display: flex;
  position: absolute;
  right: 55%;
`;

export const StyledTitle = styled.div`
  font-style: normal;
  font-weight: bold;
  font-size: 13px;
  line-height: 16px;
  text-align: center;
  color: #edcd90;
  margin-right: 4vw;
  cursor: pointer;
`;

export const StyledLogo = styled.div`
  position: absolute;
  align-self: normal;
`;

export const StyledLogoHidden = styled.div`
  position: relative;
  align-self: normal;
`;

export const StyledMainLogo = styled.img`
  position: absolute;
  left: 50%;
  top: 10%;
  z-index: 1;
  transform: translate(-50%, 0);
`;

export const StyledSubLogo = styled.img`
  transform: translateY(-4px);
`;

export const StyledTitleRight = styled.div`
  display: flex;
  position: absolute;
  left: 59%;
`;

export const StyledAccount = styled.div`
  position: absolute;
  font-style: normal;
  font-weight: bold;
  font-size: 12px;
  line-height: 16px;
  text-align: center;
  color: #fff;
  right: 3vw;
  display: flex;
  align-items: end;
  img {
    margin-left: 0.25rem;
  }
`;

export const StyledWrapperMobile = styled.div`
  background: #3b1822;
  border-bottom: 2px solid #edcd90;
  border-top: 2px solid #3b1822;
  position: relative;
  height: 76px;
  z-index: 3;
`;

export const StyledLogoMobile = styled.div`
  position: relative;
  margin-left: 22px;
`;

export const StyledMainLogoMobile = styled.img`
  position: absolute;
  left: 50%;
  top: 20%;
  z-index: 2;
  transform: translate(-50%, 0);
`;

export const StyledSubLogoMobile = styled.img`
  transform: translateY(-4px);
`;

export const StyleMemuIcon = styled.div`
  display: flex;
  align-items: center;
  height: 100%;
  position: absolute;
  z-index: 2;
  right: 32px;
  color: #fff;
`;

export const StyledTitleMobile = styled.div`
  font-style: normal;
  font-weight: bold;
  font-size: 13px;
  line-height: 16px;
  text-align: center;
  color: #edcd90;
  cursor: pointer;
  margin-top: 35px;
  &:first-child {
    margin-top: 50px;
  }
  &:last-child {
    margin-bottom: 100px;
  }
`;

export const StyledMenu = styled.div`
  background-color: #3b1822;
  display: flex;
  flex-direction: column;
  position: absolute;
  z-index: 2;
  width: 100%;
`;
