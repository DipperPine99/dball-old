import { useAppSlice } from 'app/slice';
import { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import {
  fetchAllowance,
  fetchWalletBalance,
  startRefreshingWalletBalance,
} from 'utils/updateCharacters';
// import { loadWeb3 } from 'utils/web3/connect';
import Web3 from 'web3';

export const useWallet = () => {
  const { actions } = useAppSlice();
  const dispatch = useDispatch();

  const loadWeb3 = useCallback(async () => {
    const _window: any = window;
    if (_window.ethereum) {
      const web3 = new Web3(_window.ethereum);
      _window.web3 = web3;
      await _window.ethereum.enable();
      return web3;
    } else if (_window.web3) {
      const web3 = new Web3(_window.web3.currentProvider);
      _window.web3 = web3;
      return web3;
    } else {
      window.alert(
        'Non-Ethereum browser detected. You should consider trying MetaMask!',
      );
      const error = new Error(
        'Non-Ethereum browser detected. You should consider trying MetaMask!',
      );
      error.name = 'Missing provider';
      throw error;
    }
  }, []);

  const loadBlockchainData = useCallback(async (web3: Web3) => {
    // Load account
    const accounts = await web3.eth.getAccounts();
    const account = accounts[0];

    // Network ID
    const networkId = await web3.eth.net.getId();
    dispatch(
      actions.setWallet({
        connected: true,
        account,
        networkId,
      }),
    );

    fetchWalletBalance(account);
    startRefreshingWalletBalance();
    fetchAllowance();
    return account;
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  const connectWallet = useCallback(async () => {
    const web3 = await loadWeb3();
    return await loadBlockchainData(web3);
  }, [loadWeb3, loadBlockchainData]);

  return { connectWallet };
};
