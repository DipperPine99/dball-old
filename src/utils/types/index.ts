import { ChallengerModel } from 'models/ChallengerModel';
import { CharacterModel } from 'models/CharacterModel';
import { BigNumber } from 'bignumber.js';

export type Nullable<T> = T | null;

export interface CharacterInterface {
  id: number;
  name?: string;
  rare?: number;
  exp?: number;
  level?: number;
  classId?: number;
  hatched?: boolean;
  stamina?: number;
  extraStamina?: number;
  price?: number;
}

export interface ChallengerInterface {
  id?: number;
  name?: string;
  classId?: number;
  power?: number;
  expMax?: number;
  expMin?: number;
  rewardMax?: BigNumber;
  rewardMin?: BigNumber;
  winRate?: number;
}

export interface ChallengerItemProps {
  challenger: ChallengerModel;
}

export interface CharacterItemProps {
  char: CharacterModel;
}

export interface WalletComponentProps {
  // [key: string]: any
}

export interface FightOutcome {
  ballGain: BigNumber;
  isWin: boolean;
  target: number;
  xpGain: number;
  _tokenId: number;
}
