import { ChainId } from 'config/constants';
import tokens from 'config/constants/tokens';

export interface AddressInterface {
  [ChainId.TESTNET]?: string;
  [ChainId.MAINNET]: string;
}

export const getChain = (): number => {
  return process.env.REACT_APP_CHAIN_ID
    ? parseInt(process.env.REACT_APP_CHAIN_ID)
    : ChainId.MAINNET;
};

export const getAddress = (
  address: AddressInterface,
  chainId?: number,
): string => {
  return address[chainId ?? getChain()] ?? '';
};

export const getDballAddress = (chainId?: number) => {
  return getAddress(tokens.dball.address, chainId);
};
