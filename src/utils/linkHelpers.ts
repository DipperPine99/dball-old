import { BASE_EXCHANGE_URL, ChainId } from '../config/constants';

export const getExchangeLink = (address: string, chainId: ChainId) => {
  let link: string = BASE_EXCHANGE_URL[chainId];
  return link.replace('%OUTPUT_CURRENTCY%', address);
};
