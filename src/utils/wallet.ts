import BigNumber from 'bignumber.js';
import Web3 from 'web3';
import { round } from 'lodash';

export const formatWalletAddress = (address: string) => {
  return address.substr(0, 6) + '...' + address.substr(-4, 4);
};

export const formatAmount = (amount: BigNumber, decimalPoint: number = 4) => {
  const amountDecimal = Web3.utils.fromWei(amount.toJSON(), 'ether');
  return round(amountDecimal, decimalPoint);
};

export const formatAmountNumber = (
  amount: number,
  decimalPoint: number = 4,
) => {
  const amountDecimal = Web3.utils.fromWei(amount.toString(), 'ether');
  return round(amountDecimal, decimalPoint);
};
