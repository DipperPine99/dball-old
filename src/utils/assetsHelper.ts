import {
  AVAILABLE_HERO_COUNT,
  AVAILABLE_VILLAIN_COUNT,
  RARE_COUNT,
} from '../config/constants';

const heroImages: any = [];

for (let i = 1; i <= AVAILABLE_HERO_COUNT; i++) {
  for (let j = 1; j <= RARE_COUNT; j++) {
    if (!heroImages[i]) {
      heroImages[i] = [];
    }
    heroImages[i][j] = require('assets/images/heroes/hero' +
      i +
      '/rare' +
      j +
      '.png').default;
  }
}

export const getHeroImageByKey = (id: number, rare: number) => {
  return heroImages[id][rare];
};

const villainImages: any = [];

for (let i = 1; i <= AVAILABLE_VILLAIN_COUNT; i++) {
  villainImages[i] = require('assets/images/villains/enemy' +
    i +
    '.png').default;
}

export const getVillainImageByKey = (id: number) => {
  return villainImages[id];
};
