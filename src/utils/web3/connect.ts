import Web3 from 'web3';

export const loadWeb3 = async () => {
  const _window: any = window;
  if (_window.ethereum) {
    _window.web3 = new Web3(_window.ethereum);
    const accounts = await _window.ethereum.enable();
    return accounts[0];
  } else if (_window.web3) {
    _window.web3 = new Web3(_window.web3.currentProvider);
  } else {
    window.alert(
      'Non-Ethereum browser detected. You should consider trying MetaMask!',
    );
    const error = new Error(
      'Non-Ethereum browser detected. You should consider trying MetaMask!',
    );
    error.name = 'Missing provider';
    throw error;
  }
};
