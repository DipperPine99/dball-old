import { appActions } from 'app/slice';
import { charactersService } from 'services/smartcontracts/CharactersService';
import { gameService } from 'services/smartcontracts/GameService';
import { tokenService } from 'services/smartcontracts/TokenService';
import { store } from 'store/configureStore';

const INTERVAL_TIME = 10 * 1000;
const INTERVAL_TIME_FAST = 5 * 1000;

export const fetchOwnedCharacters = async account => {
  let characters;
  characters = await charactersService.getOwnedTokenIds(account);
  // characters = await charactersService.listNFTOf(account);
  // characters = await dragonService.listCharacterOf(account);
  if (characters) {
    characters.map(async char => {
      fetchCharacterInfo(char.id);
    });
    store.dispatch(appActions.setOwnedCharacters(characters));
  }
};

export const fetchWalletBalance = async (account: string) => {
  const balance = await tokenService.balanceOf(account);
  if (balance) {
    store.dispatch(appActions.setWalletBalance(balance));
  }
};

export const fetchAllowance = async () => {
  const allowance = await tokenService.getAllowance();
  if (allowance) {
    store.dispatch(appActions.setWalletAllowance(allowance));
  }
};

export const fetchCharacterInfo = (id: number) => {
  fetchCharacterMainInfo(id);
  fetchExtraStamnia(id);
};

export const fetchCharacterMainInfo = async (id: number) => {
  const info = await gameService.getInfo(id);
  if (info) {
    info.id = id;
    store.dispatch(appActions.updateCharacterInfo({ id, info }));
    return info;
  }
};

export const fetchExtraStamnia = async (id: number) => {
  const extraStamina = await gameService.getExtraStaminaOf(id);
  if (extraStamina) {
    store.dispatch(
      appActions.updateCharacterInfo({
        id,
        info: {
          extraStamina,
        },
      }),
    );
    return extraStamina;
  }
};

let interval: NodeJS.Timeout;

export function startRefreshingCharacters(time: number = INTERVAL_TIME) {
  interval = setInterval(fetchOwnedCharacters, time);
}

export function stopRefreshingCharacters() {
  clearInterval(interval);
}

let intervalRefreshingWalletBalance: NodeJS.Timeout;

export function startRefreshingWalletBalance(
  time: number = INTERVAL_TIME_FAST,
) {
  intervalRefreshingWalletBalance = setInterval(() => {
    const account = store.getState().app.wallet.account;
    if (account) {
      fetchWalletBalance(account);
    }
  }, time);
}

export function stopRefreshingWalletBalance() {
  clearInterval(intervalRefreshingWalletBalance);
}
